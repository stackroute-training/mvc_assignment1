﻿using Microsoft.AspNetCore.Mvc;
using mvc_assignment1.Models;
using System.Collections.Generic;

namespace mvc_assignment1.Controllers
{
    public class EmployeeController : Controller
    {
        public IActionResult Index() //gets all employee details
        {
            List<Employee> emp = new()
            {
                new Employee(){id=1,name="poco",Joindate="9-2-22",email="poco@gmail.com",mobile="1234567",city="delhi"},
                new Employee(){id=2,name="el primo",Joindate="9-2-22",email="elprimo@gmail.com",mobile="1234567",city="delhi"},
                new Employee(){id=3,name="nita",Joindate="9-2-22",email="nita@gmail.com",mobile="1234567",city="delhi"},
                new Employee(){id=4,name="darryl",Joindate="9-2-22",email="darryl@gmail.com",mobile="1234567",city="delhi"}
            }; 
            return View(emp);
        }

        public IActionResult Details() //get a single employee data
        {
            Employee emp = new() { id = 1, name = "poco", Joindate = "9-2-22", email = "poco@gmail.com", mobile = "1234567", city = "delhi" };
            return View(emp);
        }
    }
}
