﻿namespace mvc_assignment1.Models
{
    public class Employee
    {
        public int id  { get; set; }
        public string name { get; set; }
        public string Joindate { get; set; }
        public string email { get; set; }
        public string  mobile { get; set; }

        public string city { get; set; }
    }
}
